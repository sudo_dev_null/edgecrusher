import argparse
import logging
import os
from cterasdk import *
from smb.SMBConnection import SMBConnection
import threading
import time
from dummy_file_generator import DummyFileGenerator as Dfg, DummyFileGeneratorException

config.Logging.get().setLevel(logging.WARNING)

def make_data():
    logging_level = "INFO"
    project_scope_kwargs = {
        "project_name": "dummy1",
        "default_rowcount": None,
    }   

    try:
        dfg_obj = Dfg(logging_level, **project_scope_kwargs)
    except DummyFileGeneratorException as DFG_ERR:
        raise DFG_ERR
    file_scope_kwargs = {
        "generated_file_path": "S:\dummy1\\file1.csv",
        "file_size": 1024,
        #"row_count": None,
        "file_encoding": "utf8",
        "file_line_ending": "\n",
    }

    try:
        dfg_obj.write_output_file(**file_scope_kwargs)
    except DummyFileGeneratorException as DFG_ERR:
        raise DFG_ERR



def make_user_folder(user_name):
    semaphore.acquire()
    conn = SMBConnection(user_name, arguments.password, arguments.target_share, arguments.target_device_name, use_ntlm_v2=True)
    assert conn.connect(arguments.target_ip, 139)    
    conn.createDirectory(arguments.target_share, user_name)
    semaphore.release()
            

def open_and_store(share_name, user_name, file_name, base_name):
    semaphore.acquire()
    target_path = user_name + '/' + base_name
    conn = SMBConnection(user_name, arguments.password, arguments.target_share, arguments.target_device_name, use_ntlm_v2=True)
    assert conn.connect(arguments.target_ip, 139)    
    
    with open(file_name, 'rb') as file:
        config.logging.warning('Storing file: ' + share_name + '/' + target_path)
        conn.storeFile(share_name, target_path, file)
    semaphore.release()

def run_user_thread(user):
    file_counter = 0
    for subdir, dirs, files in os.walk(arguments.source_dir):
        for file in files:
            file_path = subdir + os.sep + file
            if (not arguments.is_dryrun and file_counter < arguments.max_files):
            #if (file_path.endswith(".mp3")):
                open_and_store(arguments.target_share, user, file_path, os.path.basename(file))
                file_counter += 1
            else:
                pass
    

if __name__ == "__main__":
    try:
        parser = argparse.ArgumentParser(description='SMB Stress Tester')
        parser.add_argument('-mt', '--max-threads', action='store', dest='max_threads', default=10, type=int, help='Maximum number of concurrent file threads (Default: 10)')
        parser.add_argument('-mu','--max-users', action='store', dest='max_users', default=1, type=int, help='Number of users to process from user file (Default: 1)')
        parser.add_argument('-mf','--max-files', action='store', dest='max_files', default=100, type=int, help='Maximum number of files to write per user (Default: 100)')
        parser.add_argument('-f', '--user-file', action='store', dest='user_file_name', default='ad_users.txt', type=str, help='Location of the line delimited user name file (Default: ad_users.txt)')
        parser.add_argument('-s', '--target-share', action='store', dest='target_share', type=str, required=True, help='Target SMB share to read from or write to')
        parser.add_argument('-i', '--target-ip', action='store', dest='target_ip', type=str, required=True, help='IP address or DNS name of target device')
        parser.add_argument('-d', '--target-device-name', action='store', dest='target_device_name', type=str, required=True, help='hostname of the target device (non-FQDN)')
        parser.add_argument('-S', '--source-dir', action='store', dest='source_dir', type=str, default='./', help='Directory to copy data from (Example: S:) (Default: running directory)')
        parser.add_argument('-p', '--password', action='store', dest='password', type=str, default='Password1', help='Single Password for users in list (Default: Password1 )')
        parser.add_argument('--dryrun', action='store_true', dest='is_dryrun', default=False, help='Only creates user folders, prevents file transfers')
        parser.add_argument('--make-data', action='store_true', dest='is_make_data', default=False, help='Makes sample data to use in transfer')
        parser.add_argument('--debug', action='store_true', dest='is_debug', default=False, help='Enables debug output of arguments')

        arguments = parser.parse_args()

        if (arguments.is_debug):
            print('max_threads =', arguments.max_threads)
            print('max_users =', arguments.max_users)
            print('max_files =', arguments.max_files)
            print('user_file_name =', arguments.user_file_name)
            print('target_share =', arguments.target_share)
            print('target_ip =', arguments.target_ip)
            print('target_device_name =', arguments.target_device_name)
            print('source_dir =', arguments.source_dir)
            print('password =', arguments.password)
            print('is_dryrun =', arguments.is_dryrun)
            print('is_make_data =', arguments.is_make_data)
            print('is_debug =', arguments.is_debug)
       
        if (arguments.is_make_data):
            make_data()
        
        semaphore = threading.Semaphore(value=arguments.max_threads)
        thread_pool = list()
        user_file = open(arguments.user_file_name, 'r')
        users = user_file.readlines()
        
        user_count = 0
        for user in users:
            if (user_count < arguments.max_users):
		user_count += 1
                user_thread = threading.Thread(target=make_user_folder, args=(user.strip(),))
                thread_pool.append(user_thread)
                user_thread.start()

        user_count = 0  #reset count for new loop
        for user in users:
            if (user_count < arguments.max_users):
                user_count += 1
                user_thread = threading.Thread(target=run_user_thread, args=(user.strip(),))
                thread_pool.append(user_thread)
                user_thread.start()
         
    except KeyboardInterrupt as error:
        pass
