import logging
import argparse
from smb.SMBConnection import SMBConnection
config.Logging.get().setLevel(logging.WARNING)
import uuid

def make_folders():
    
    conn = SMBConnection(arguments.user, arguments.password, arguments.target_share, arguments.target_device_name, use_ntlm_v2=True)
    assert conn.connect(arguments.target_ip, 139)    
    for i in range(arguments.folder_count):
        folder_name = 'this_is_my_folder_test/' + str(uuid.uuid4())
        conn.createDirectory(arguments.target_share, folder_name)
        config.logging.warning('Folder number ' + str(i+1) + ' of ' + str(arguments.folder_count) + ' created: ' + folder_name)


if __name__ == "__main__":
    try:
        parser = argparse.ArgumentParser(description='SMB Random Folder Maker')
        parser.add_argument('-c', '--count', action='store', dest='folder_count', default=10, type=int, help='Number of folders to create (Default: 10)')
        parser.add_argument('-s', '--target-share', action='store', dest='target_share', type=str, required=True, help='Target SMB share to read from or write to')
        parser.add_argument('-i', '--target-ip', action='store', dest='target_ip', type=str, required=True, help='IP address or DNS name of target device')      
        parser.add_argument('-d', '--target-device-name', action='store', dest='target_device_name', type=str, required=True, help='hostname of the target device (non-FQDN)')
        parser.add_argument('-u', '--user', action='store', dest='user', type=str, required=True, help='User to create the folders')
        parser.add_argument('-p', '--password', action='store', dest='password', type=str, default='Password1', help='Single Password for users in list (Default: Password1 )')
        parser.add_argument('--debug', action='store_true', dest='is_debug', default=False, help='Enables debug output of arguments')

        arguments = parser.parse_args()

        if (arguments.is_debug):
            print('folder_count =', arguments.folder_count)
            print('target_share =', arguments.target_share)
            print('target_ip =', arguments.target_ip)
            print('target_device_name =', arguments.target_device_name)
            print('user =', arguments.user)
            print('password =', arguments.password)
            print('is_debug =', arguments.is_debug)
        
        make_folders()
            
    except KeyboardInterrupt as error:
        pass